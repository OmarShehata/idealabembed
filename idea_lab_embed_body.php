<?php

class IdeaLabEmbed {
	static function onParserInit( Parser $parser ) {
		$parser->setHook( 'idea', array( __CLASS__, 'ideaLabEmbedRender' ) );
		return true;
	}
	static function ideaLabEmbedRender( $input, array $args, Parser $parser, PPFrame $frame ) {
		//Takes the URL of an idea and automatically structures a nice embedded box
		$page_content = file_get_contents($args['url']);
		$dom = new DOMDocument();
		$dom->loadHTML($page_content);
		//Parse data of the idea
		$xpath = new DOMXpath($dom);
		//Get the title node
		$title_node = $xpath->query('//*[@id="mw-content-text"]/div[1]/div[2]/div[1]')->item(0);
		$idea_title = $title_node->ownerDocument->saveHTML( $title_node );
		$idea_title_name = substr($title_node->nodeValue,1); //This is a hack to fix the mysterious resilient <p> tag
		// Get the description
		$desc_node = $xpath->query('//*[@id="mw-content-text"]/div[1]/div[2]/div[2]/div[2]/span')->item(0);
		$idea_desc = $desc_node->ownerDocument->saveHTML( $desc_node );

		//Header
		$ret = '<div style="max-width:400px; display:inline-block; margin-bottom:15px; ">';
		$ret .= '<div style="background-color: #cd6601; padding: .25em 0 0 1em; font-size: 1.3em">
					<div style="display:inline; padding-right:.25em;;display:inline; padding-right:.25em;"><a href="'.$args['url'].'" title="Grants:IdeaLab"><img alt="IEG labcat white.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f9/IEG_labcat_white.svg/25px-IEG_labcat_white.svg.png" width="25" height="23" style="vertical-align: top" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/f/f9/IEG_labcat_white.svg/38px-IEG_labcat_white.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/f/f9/IEG_labcat_white.svg/50px-IEG_labcat_white.svg.png 2x" data-file-width="195" data-file-height="181"></a></div>
					<div style="display:inline; word-wrap:break-word;;display:inline; word-wrap:break-word;"><span style=""><a href="'.$args['url'].'" title="Grants:IdeaLab"><span style="color:#FFFFFF">'. $idea_title_name . ' - from the Idealab</span></a></span></div>
					</div>';
		// Info box
		$ret .= '<div style="background-color: #eeeeee; color: #000000; border-left: 1px solid #ddd; border-top: 1px solid #ddd; border-right: 1px solid #ddd; border-bottom: 3px solid #D0D0D0; border-radius: 1px; padding: 12px 20px 20px 20px;">';
		$ret .= $idea_title;
		$ret .= $idea_desc;
		$ret .= '</div></div>';

		return $ret;
	}
}
