# IdeaLabEmbed #

This is a MediaWiki extension for connecting the learning patterns to the idea lab. See: https://medium.com/@omar4ur/connecting-wikimedias-idea-lab-learning-patterns-resource-3f35a42b2218

### Install ###

To install, simple clone this repository in the extensions folder, and then put this in your LocalSettings.php:

```
require_once( "$IP/extensions/IdeaLabEmbed/idea_lab_embed.php" );
```

### Example Usage ###

To embed an idea into a page, just use the `idea` tag like so:

```
<idea url="https://meta.wikimedia.org/wiki/Grants:IdeaLab/Make_it_easy_to_embed_Wikimedia_content"></idea>
```

This will embed an info box with a link to that idea page.